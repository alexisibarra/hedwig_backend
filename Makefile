systemuser="root"
hostname="localhost"
databasename="laiguana_hedwig"
username="laiguana_hedwig"
password="laiguana_hedwig"

run:
	slc run .
set-env:
	cp server/config.development{.${CLIENT},}.json
	cp server/config.production{.${CLIENT},}.json
	cp server/datasources.development{.${CLIENT},}.js
	cp server/datasources.production{.${CLIENT},}.js
config-local-db:
	mysql -h ${hostname} -u ${systemuser} -p -e "CREATE DATABASE IF NOT EXISTS ${databasename};"
	mysql -h ${hostname} -u ${systemuser} -p -e "CREATE USER IF NOT EXISTS '${username}'@'${hostname}' IDENTIFIED BY '${password}';"
	mysql -h ${hostname} -u ${systemuser} -p -e "GRANT ALL PRIVILEGES ON ${databasename}.* TO '${username}'@'${hostname}' IDENTIFIED BY '${password}';"