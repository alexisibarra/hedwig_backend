# Hedwig

Editor de contenidos para Jasolina. Generado por [LoopBack](http://loopback.io). Desarrollado por [Alexis Ibarra](https://github.com/alexisibarra)

## Requerimientos

Este proyecto asume que las siquientes herramientas están instaladas:

* [NodeJS 0.12+](https://nodejs.org)
* [Foreman](https://github.com/strongloop/node-foreman)
* [Loopback Framework](http://loopback.io)

A su vez, es necesaria la disponibilidad de:

  * Un bucket de S3 para la carga de imágenes
  * Una instancia de MySQL para la persistencia de datos.
  * Existencia de la variable de entorno `NODE_ENV`. Esto es configurado automáticamente al hacer uso de [Foreman](https://github.com/strongloop/node-foreman).

    Esta variable puede tener alguno de los siguientes valores:

    * "development"
    * "production"
    * "staging"

# Instalación

1. Clone el código base desde el repositorio:

    $ git clone ssh://git-codecommit.us-east-1.amazonaws.com/v1/repos/hedwig-backend

2. Instale todas las dependencias

    $ npm install

3. Copie el archivo `/server/aws-credentials.example.json` a `/server/aws-credentials.json` y edite el contenido con el acessKeyId, secretAccessKey y la region del bucket AWS detinado a almacenar las imágenes de los artículos

4. Dependiendo del ambiente en el cual vaya a hacer el despliegue, copie los siguientes archivos y configurelos adecuadamente
    * `/server/config.example.json` a `/server/config.<enviroment>.js`
    * `/server/datasources.example.js` a `/server/datasources.<enviroment>.js`

5. Cree y configure el archivo .env. Debe tener el siguiente formato:

  $ item_type= ""
  $ NODE_ENV= ""
  $ s3_bucket= ""

Donde

  - item_type: tipo de item soportado por Hedwig: 'li-news' o 'sg-articles'
  - NODE_ENV: ambiente en el que se ejecutará la aplicación: 'development', 'production' o 'staging'
  - s3_bucket: bucket s3 para subir imagenes de items

En este punto tendrá todo listo para ejecutar el proyecto.

# Ejecución (En desarrollo)

Hay dos opciones para ejecutar el proyecto. La primera consiste en ejecutar el siguiente comando:

    $ node .

La segunda opción (recomendada) es usando [Foreman](https://github.com/strongloop/node-foreman):

    $ nf start

Esto cargará el archivo `.env` y configurará las variables de entorno necesarias para que la aplicación funcione adecuadamente.

# Ejecución (En producción)

Para ejecutar en producción, la variable de entorno `NODE_ENV` debe ser configurada y luego ejecutar el siguiente comando:

    $ slc start
