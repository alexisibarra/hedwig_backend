module.exports = {

  InvalidImageUpload: function () {
    error = new Error();
    error.statusCode = 498;
    error.status = 498;
    error.name = "InvalidImageUpload";
    error.message = 'The image could not be uploaded';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },

  InvalidModelName: function () {
    error = new Error();
    error.statusCode = 404;
    error.status = 404;
    error.name = "InvalidModelName";
    error.message = 'The model name is invalid';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },

  InvalidModelId: function (id) {
    error = new Error();
    error.statusCode = 404;
    error.status = 404;
    error.name = "InvalidModelId";
    error.message = 'Could not find a model with id ' + id;

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },

  InvalidInfopointType: function (model) {
    error = new Error();
    error.statusCode = 404;
    error.status = 404;
    error.name = "InvalidInfopointType";
    error.message = model + ' is not an infopoint type';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },

  FileNotFound: function () {
    error = new Error();
    error.statusCode = 400;
    error.status = 400;
    error.name = "FileNotFound";
    error.message = 'File not found';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },

  InstallationAlreadyExist: function () {
    error = new Error();
    error.statusCode = 422;
    error.status = 422;
    error.name = "InstallationAlreadyExist";
    error.message = 'Installation object already exists';

    return error;
  },

  InstallationNotFound: function () {
    error = new Error();
    error.statusCode = 404;
    error.status = 404;
    error.name = "InstallationNotFound";
    error.message = 'Installation not found';

    return error;
  },

  UserIsNotLoggedIn: function () {
    error = new Error();
    error.statusCode = 422;
    error.status = 422;
    error.name = "UserIsNotLoggedIn";
    error.message = 'User is not logged in';

    return error;
  },

  UnexpectedError: function () {
    error = new Error();
    error.statusCode = 500;
    error.status = 500;
    error.name = "UnexpectedError";
    error.message = 'An Unexpected Error has Ocurred';

    return error;
  },

  InvalidOS: function () {
    error = new Error();
    error.statusCode = 422;
    error.status = 422;
    error.name = "InvalidOS";
    error.message = 'The provided OS is invalid';

    return error;
  },

  NoPreferencesFoundForUser: function () {
    error = new Error();
    error.statusCode = 404;
    error.status = 404;
    error.name = "NoPreferencesFoundForUser";
    error.message = 'No preferences where found for this user';

    return error;
  },

  InvalidBoatId: function () {
    error = new Error();
    error.statusCode = 400;
    error.status = 400;
    error.name = "InvalidBoatId";
    error.message = 'boatId property is invalid or was not found';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },
  InvalidServiceProviderId: function () {
    error = new Error();
    error.statusCode = 400;
    error.status = 400;
    error.name = "InvalidServiceProviderId";
    error.message = 'serviceProviderId property is invalid or was not found';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },
  InvalidAccessToken: function () {
    error = new Error();
    error.statusCode = 400;
    error.status = 400;
    error.name = "InvalidAccessTokenId";
    error.message = 'The AccessToken is invalid or was not found';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  },
  InvalidEmail: function () {
    error = new Error();
    error.statusCode = 400;
    error.status = 400;
    error.name = "InvalidEmailId";
    error.message = 'email property is invalid or was not found';

    return {
      'statusCode': error.status,
      'status': error.status,
      'name': error.name,
      'message': error.message
    };
  }


}
