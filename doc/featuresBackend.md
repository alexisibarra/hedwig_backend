# Features del backend

## Manejo de sesión y perfil de usuario
- Inicio de sesión
    + Si es la primera vez que inicia sesión debe obligar al usuario a cambiar la contraseña
- Cierre de sesión
- Recuperación de contraseña
- Cambio de contraseña

## Manejo de usuarios
- Creación de usuarios
    + Se debe crear con una contraseña aleatoria
    + Se debe crear con la obligación de que el usuario cambie la contraseña al hacer el primer login
    + Al crear el usuario este debe estar desactivado hasta que la contraseña sea cambiada
- Obtener todos los usuarios
    + Se debe poder diferenciar entre usuarios activos y no activos
- Obtener usuario en detalle
- Actualizar usuario
- Eliminar usuario

## Manejo de artículos
- Creación de artículo
- Obtener todos los artículos
- Obtener artículo
- Actualizar artículo
- Eliminar artículo

### Manejo de multimedia
#### Manejo de imagenes
+ Carga de imagen
+ Configuración de AWS
+ Transcoding de la imagen
+ Obtener todas las imagenes disponibles
+ Obtener imagen
+ Eliminar imagen

#### Manejo de multimedia social
- Cargar un tweet
- Obtener un tweet
- Actualizar un tweet
- Cargar un video de youtube
- Obtener un video de youtube
- Obtener todos los videos de youtube
- Actualizar un video de youtube
- Eliminar elemento de multimedia social

## Manejo de categorias
- Creación de categoría
- Obtener categoría
- Obtener categorias
- Editar categoría
- Eliminar categoría
