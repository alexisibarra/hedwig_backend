var credentials = require('./aws-credentials.json');

module.exports = {
  "image-container": {
    "name": "image-container",
    "connector": "loopback-component-storage",
    "maxFileSize": "52428800",
    "provider": "amazon",
    "key": credentials.secretAccessKey,
    "keyId": credentials.accessKeyId,
    "getFilename": function(fileInfo) {
      var fileName = fileInfo.name.replace(/\s+/g, '-').toLowerCase();
      return 'image-' + new Date().getTime() + '-' + fileName;
    }
  },
  "files-container": {
    "name": "files-container",
    "connector": "loopback-component-storage",
    "provider": 'filesystem',
    "root": './uploads'
  },
  "mysql": {
    "host": "localhost",
    "port": 3306,
    "url": "",
    "database": "DATABASENAME",
    "user": "DATABASEUSER",
    "password": "DATABASEPASSWORD",
    "name": "mysql",
    "connector": "mysql"
  },
  "emailDS": {
    "name": "emailDS",
    "connector": "mail",
    "transports": [
      {
        "type": "smtp",
        "host": "smtp.gmail.com",
        "secure": true,
        "port": 465,
        "tls": {
          "rejectUnauthorized": false
        },
        "auth": {
          "user": "EMAILADDRESS",
          "pass": "EMAILPASSWORD"
        }
      }
    ]
  }
};
