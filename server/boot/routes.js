// var dsConfig = require('../datasources.json');
var _ = require('lodash');
var crypto = require('crypto');

module.exports = function (app) {
  var User = app.models.EditorUser;

  app.get('/api/', function(req, res, cb){
    var Config = app.models.Config;

    return Config
      .find()
      .then(function(config){
        console.log(config);
        return res.status(200).send({status: 200, message: "OK", config: app.settings.frontend, home_active: config[0].home_active, article_active: config[0].article_active});
      })
  });

  app.post('/api/request-password-reset', function(req, res, next) {
    User.resetPassword({
      email: req.body.email
    }, function(err) {
      if (err) return res.status(401).send({message: "Correo electrónico no encontrado"});

      return res.status(200).send({message: "Verifique su correo electrónico para mas información"});
    });
  });

  app.post('/api/reset-password', function(req, res, next) {
    if (!req.body.accessToken) return res.sendStatus(401);

    if (!req.body.password ||
      !req.body.confirmation ||
      req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    // TODO: eliminar el token

    app.models.AccessToken.findById(req.body.accessToken)
      .then(function(accessToken) {
        return User.findById(accessToken.userId)
      })
      .then(function(user) {
        return user.updateAttribute('password', req.body.password);
      })
      .then(function(response){
        return app.models.AccessToken.destroyById(req.body.accessToken)
      })
      .then(function(response){
        return res.status(200).send({message: "Su contraseña ha sido reestablecida con éxito"});
      })
      .catch(function(err){
        return res.status(401).send({message: "El token de reestablecimiento de contraseña ha expirado. Debe reestablecer su contraseña nuevamente."});
      });
  });
}
