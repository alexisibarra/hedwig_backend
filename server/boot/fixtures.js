var loopback = require('loopback');
var _ = require('lodash');
var Q = require('q');

module.exports = function (app) {

  var User = app.models.User;
  var Item = app.models.Item;
  var Categorization = app.models.Categorization;
  var EditorUser = app.models.EditorUser;
  var Role = app.models.Role;
  var Config = app.models.Config;
  var RoleMapping = app.models.RoleMapping;

  var categorizations = [];

  var config = require('../config.' + process.env.NODE_ENV + '.json').backend;

  if(config.client_name === "La Iguana"){
    categorizations = [
      { "type": "categoria", "body": "Política y sexo", "position": 1},
      { "type": "categoria", "body": "Economía y faranduleo", "position": 2},
      { "type": "categoria", "body": "Mundo y deportes", "position": 3},
      { "type": "categoria", "body": "Cultura y servicios", "position": 4},
      { "type": "categoria", "body": "Sucesos y tecnología", "position": 5},
      { "type": "categoria", "body": "Virales y salud", "position": 6},

      { "type": "zona", "body": "Destacados", "position": 1},
      { "type": "zona", "body": "C1", "position": 2},
      { "type": "zona", "body": "C2", "position": 3},
      { "type": "zona", "body": "C3", "position": 4},
      { "type": "zona", "body": "C4", "position": 5},
      { "type": "zona", "body": "C5", "position": 6},
      { "type": "zona", "body": "lo+jot", "position": 7},
      { "type": "zona", "body": "Lo+Polémico", "position": 8},
      { "type": "zona", "body": "Iguanazos", "position": 9},
      { "type": "zona", "body": "La Foto", "position": 10},
      { "type": "zona", "body": "Cara a Cara", "position": 11},

      { "type": "censura", "body": "A", "position": 1},
      { "type": "censura", "body": "B", "position": 1},
    ];
  }  else if(config.client_name === "Solo Genuino"){
    console.log("categorizaciones solo genuino");
    categorizations = [
      { "type": "categoria", "body": "Cat1"},
      { "type": "categoria", "body": "Cat2"},
      { "type": "zona", "body": "Zone1"},
      { "type": "zona", "body": "Zone2"}
    ]
  }

  _.each(categorizations, function(category){
    Categorization.findOrCreate({body: category.body}, category)
      .then(function (category) {
        console.log("Created categorization", category);
      })
      .catch(function (err) {
        cb(err);
      });
  });

  Config.findOrCreate({active: true}, {active: true})
      .then(function (config) {
        console.log("Created configuration", config);
      })
      .catch(function (err) {
        cb(err);
      });

  Config.findOrCreate({active: true}, {active: true})
      .then(function (config) {
        console.log("Created configuration", config);
      })
      .catch(function (err) {
        cb(err);
      });

};
