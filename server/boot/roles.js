var _ = require('lodash');
var q = require('q');

module.exports = function (app) {

  // Create superadmin user and role
  var adminUser = {};
  var adminRole = {};
  var chiefUser = {};
  var chiefRole = {};

  q.all([
    app.models.EditorUser.findOrCreate(
      {where: {'email': 'admin@cambiame.com'}},
      {
        "fullname": "Admin",
        "username": "admin",
        "email": "admin@cambiame.com",
        "password": "admin",
        "password_change": false,
        "active": false,
        "emailVerified": true
      }),
    app.models.Role.findOrCreate({name: 'admin', 'description': 'Administrador'}),
    app.models.EditorUser.findOrCreate(
      {where: {'email': 'chief@cambiame.com'}},
      {
        "fullname": "Jefe Editores",
        "username": "chief",
        "email": "chief@cambiame.com",
        "password": "chief",
        "password_change": false,
        "active": false,
        "emailVerified": true
      }),
    app.models.Role.findOrCreate({name: 'chief', 'description': 'Jefe de Editores'})
  ])
  .then(function (array) {
    adminUser = array[0][0];
    adminRole = array[1][0];
    chiefUser = array[2][0];
    chiefRole = array[3][0];

    console.log(array);

    return q.all([
      app.models.RoleMapping.find( { where: {
        principalType: app.models.RoleMapping.USER,
        principalId: adminUser.id,
      }}),
      app.models.RoleMapping.find( { where: {
        principalType: app.models.RoleMapping.USER,
        principalId: chiefUser.id}
      })
    ]);
  })
  .then(function (roleMapping) {
    let promises = [];

    if (roleMapping[0].length <= 0){
      promises.push(adminRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: adminUser.id,
        roleId: adminRole.id        
      }));
    }

    if (roleMapping[1].length <= 0){
      promises.push(chiefRole.principals.create({
        principalType: app.models.RoleMapping.USER,
        principalId: chiefUser.id,
        roleId: chiefRole.id        
      }));
    }
    return q.all(promises);
  })
  .then(function () {
    console.log('Finished "superadmin" user and role creation');
  })
  .catch(function (err) {
    throw err;
  });

  var users = [
    {
      "fullname": "Editor A",
      "username": "editora",
      "email": "editor@gmail.com",
      "password": "editora",
      "password_change": false,
      "active": false,
      "emailVerified": true
    },
    {
      "fullname": "Editor B",
      "username": "editorb",
      "email": "editor@gmail.com",
      "password": "editorb",
      "password_change": false,
      "active": false,
      "emailVerified": true
    },
  ];

  _.each(users, function(user){
    app.models.EditorUser.findOrCreate(
      {where: {'email': user.email}},
      user
    )
      .then(function (array) {
        var editorUser = array[0];

        console.log('Finished user creation: ', editorUser.email);
      })
      .catch(function (err) {
        throw err;
      });
  });
};
