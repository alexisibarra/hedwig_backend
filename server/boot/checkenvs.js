if(!process.env.NODE_ENV){
  throw new Error("Debe configurar adecuadamente la variable de entorno `NODE_ENV` lea el README para saber como");
}

var config = require('../config.' + process.env.NODE_ENV + '.json').backend;

if(!config.s3_images_bucket){
  throw new Error("Debe configurar adecuadamente la variable de entorno `s3_images_bucket` lea el README para saber como");
}
if(!config.item_type){
  throw new Error("Debe configurar adecuadamente la variable de entorno `item_type` lea el README para saber como");
}
