var modelsJSON = {};
var config = require('./config.' + process.env.NODE_ENV + '.json').backend;
var item_type = config.item_type;
var _ = require('lodash');

if(!item_type){
  throw new Error("Debe configurar adecuadamente la variable de entorno `item_type` lea el README para saber como");
}

modelsJSON[item_type] = {
    "dataSource": "mysql",
    "public": true
  };

_.each(config.social_media_public, function(value, key){
  modelsJSON[key] = {
    "public": value,
    "dataSource": "mysql",
  }
});

module.exports = modelsJSON;
