'use strict';

var app = require('../../server/server');
var FroalaEditor = require('wysiwyg-editor-node-sdk');
var hedwigconfig = app.settings.backend;
var credentials = require('../../server/aws-credentials.json');


module.exports = function(Image) {
  Image.getsignature = function (req, res, cb) {

    var configs = {
      bucket: hedwigconfig.s3_images_bucket,
      region: 'us-east-1',
      keyStart: 'note-body',
      acl: 'public-read',

      // AWS keys.
      accessKey: credentials.accessKeyId,
      secretKey: credentials.secretAccessKey
    }


    var s3Hash = FroalaEditor.S3.getHash(configs);

    cb(null, s3Hash);

  };

  Image.remoteMethod(
    'getsignature',
    {
      http: {path: '/getsignature', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: 'Object', root: true}
    }
  );
};
