'use strict';
var app = require('../../server/server');
var errorHandling = require("../../utils/error-handling.js");
var Q = require('q');
var XLSX = require('xlsx');
var _ = require('lodash');
var path = require('path');
var hedwigconfig = app.settings.backend;

module.exports = function(Item) {
  // Para que los hooks sean heredados es necesario configurarlos en el setup del modelo padre. Mas información en https://github.com/strongloop/loopback/issues/702
  Item.setup = function() {
    Item.base.setup.apply(this, arguments);

    this.validatesPresenceOf('creation_date');
    this.validatesLengthOf('title', {max: 100, message: {max: 'El título es muy largo'}});

    function validateDate(err, done){
      if (new Date(this.publish_date_start) > new Date(this.publish_date_end)){
        err();
      }
    }

    this.validate('publish_date_end', validateDate, {message:'La fecha de comienzo de publicación debe ser mayor a la de finalización'});

    this.beforeRemote('create', function (context, alert, next) {
      context.req.body['creation_date'] = new Date();
      context.req.body['modification_date'] = new Date();
      checkPublishDateValidity(context, next);

      next();
    });

    this.afterRemote('find', function(ctx, items, next) {
      if(!(_.isUndefined(ctx.args.filter))){

        try {
          var argsJSON = JSON.parse(ctx.args);          
        } catch (error) {
          var argsJSON = ctx.args;        
        }

        if(
            _.includes(argsJSON.filter.include, 'images')
          ){
          _.each(items, item => {
            var mandatoryNotFound = false;
            
            _(hedwigconfig.image_tags)
              .filter(tag => tag.mandatory)
              .each(function(mandatory_tag){
                var mandatoryFound = _.find(item.__data.images, function(image){
                  return image.tag === mandatory_tag.tag;
                });

                if(!mandatoryFound || mandatoryFound.length < 1){
                  mandatoryNotFound = mandatoryNotFound || true;
                }
              })

              item.mandatoryImagesNotFound = mandatoryNotFound;
          })
        }
      }



      next();
    });

    this.beforeRemote('prototype.updateAttributes', function (context, alert, next) {
      Item.findById(context.req.params.id)
        .then(function(item){
          context.req.body['modification_date'] = new Date();
          context.req.body['locked'] = false;
          
          context.req.body['publish_date_start'] = context.req.body['publish_date_start'] || item['publish_date_start'];
          context.req.body['publish_date_end'] = context.req.body['publish_date_end'] || item['publish_date_end'];

          checkPublishDateValidity(context, next);
          next();
        });
    });

    function checkPublishDateValidity(context, next){
      var publish_date_start = context.req.body['publish_date_start'];
      var publish_date_end = context.req.body['publish_date_end'];

      if (new Date(publish_date_start) > new Date(publish_date_end)){
        next(new Error("La fecha de comienzo de publicación debe ser mayor a la de finalización"));
      }
    }

    this.uploadImage = function (id, tag, req, res, cb) {
      req.params["container"] = hedwigconfig.s3_images_bucket;

      this.findById(req.params.id)
        .then(function(item){
          if (!item){
            throw errorHandling.InvalidModelId();
          } else{
            return item;
          }
        })
        .then(function(item) {
          app.models.ImageContainer.upload(req, res, function (err, result) {
            if (err) {
              cb(err);
            } else {
              item.images.create({
                tag: tag,
                url: result.files.file[0]['providerResponse']['location'],
                filename:  result.files.file[0]['providerResponse']['name']}
              )
                .then(function(item){
                  cb(null, item);
                })
                .catch(function (err) {
                  cb(err);
                });
            }
          });
        })
        .catch(function (err) {
          cb(err);
        });
    };

    function parseFile(fileDir, editorUserId) {
      var workbook = XLSX.readFile(fileDir);

      var sheet_name_list = workbook.SheetNames;
      var items = [];

      _.each(sheet_name_list, function(sheetName) {
        var worksheet = workbook.Sheets[sheetName];
        // Se ubica el rango de celdas con información útil del archivo
        var worksheetRange = _.find(worksheet, function (item, key) {
          return key === '!ref';
        }).split(':');

        var coordinateRegex = /([A-Z]+)([0-9]*)/i;
        var coordinatesInit = coordinateRegex.exec(worksheetRange[0]);
        var coordinatesEnd = coordinateRegex.exec(worksheetRange[1]);

        if( coordinatesInit[1].length > 1 || coordinatesEnd[1].length > 1) {
          console.log("No soportamos archivos con tantas columnas. Notifique al desarrollador.");
		      // TODO: hace falta soportar archivos con numero de columnas mayor a la cantidad de letras del alfabeto
          return []
        }

        var columnInit = coordinatesInit[1].charCodeAt(0);
        var rowInit = _.toInteger(coordinatesInit[2]);

        var columnEnd = coordinatesEnd[1].charCodeAt(0);
        var rowEnd = _.toInteger(coordinatesEnd[2]);

        _.each(
          _.range(columnInit, columnEnd + 1),
          columnNumber => {
            var fieldnameCoordinate = String.fromCharCode(columnNumber) + 1;
            _.each(
              _.range(rowInit + 1, rowEnd + 1),
              rowNumber => {
                var valueCoordinate = String.fromCharCode(columnNumber) + rowNumber;

                // Se eliminó una fila y xlsx comienza a contar en 1, asi que...
                rowNumber -= 2;

                if (!items[rowNumber]) {
                  items[rowNumber] = {};
                }

                var fieldname = worksheet[fieldnameCoordinate].w ;

                var value = worksheet[valueCoordinate] ? worksheet[valueCoordinate].w : '';
                var listRegex = /\[(.*)\]/g;
                var listArray = listRegex.exec(value);

                if (listArray){
                  var finalValue = listArray[0].replace(/[“”‘’]/g, '"');
                  value = JSON.parse(finalValue);
                }

                items[rowNumber][fieldname] = value;
                items[rowNumber].editorUserId = editorUserId;
              }
            );
          }
        );
      });

      return items;
    }

    function validateItems(items){
      var errorMessages = [];

      _.each(items, function(item){
        item.creation_date = new Date();

        var itemModel = app.models[hedwigconfig.item_type];

        try {
          itemModel(item).isValid(function (valid) {
            if (!valid) {
              console.log("No es válido: ", item);

              errorMessages.push(
                itemModel.create(item)
                  .then(function(item){
                    console.log("eso no debería ejecutarse nunca")
                  })
                  .catch(function(err){
                    return {itemNumber: item['#'], message: err.message};
                  })
              );
            }
          })
        } catch(err){

          var invalidDate = /Invalid date:/g.exec(err);
          var jsonString = /could not create List from JSON string:/g.exec(err);

          var message = '';

          if(invalidDate){
            message = "Invalid date";
          } else if (jsonString){
            message = "Invalid list type. Empty lists should be [].";
          } else {
            message = err;
          }

          errorMessages.push({itemNumber: item['#'], message: message});
        }
      });

      return Q.all(errorMessages);
    }

    this.massiveLoad = function (req, res, cb) {
      app.models.FilesContainer.upload(req, res, {container: "archivos"}, function (err, result) {
        if(err) {
          console.log("hubo un error", err);
          cb(err);
        }

        if (result === undefined || result === null) {
          cb("A file must be provided");
          return false
        }

        console.log(result.files);

        var filename = result.files.file[0].name;
        var uploadsFilePath = path.normalize(__dirname + "./../../uploads/archivos/");
        var filepath = path.join(uploadsFilePath, filename);

        var items = parseFile(filepath, req.params.id);

        if (items.length < 1){
          cb("Ha ocurrido un error al interpretar el archivo");
        }

        app.models.FilesContainer.removeFile("archivos", filename, function(err, result){ if (err) { throw err;} });

        validateItems(items)
          .then(function(validationErrors){
            if(validationErrors.length > 0){
              console.log("El archivo contiene errores");
              cb(validationErrors);
            } else {
              var itemModel = app.models[hedwigconfig.item_type];

              itemModel.create(items, function (err, createdModel, created) {
                if (err) {
                  console.error('error creating Model', err);
                } else {
                  console.log("successfully created:", createdModel);
                  cb(null, "successfully created");
                }
              })
            }
          });
      });
    };

    this.deleteImage = function (req, res, cb) {
      req.params["container"] = hedwigconfig.s3_images_bucket;

      var articlePromise = this.findById(req.params.id);

      app.models.Image.findById(req.params.imageID)
        .then(function(image){
          return Q.all([
            app.models.Image.findById(req.params.imageID),
            app.models.ImageContainer.removeFile(
              hedwigconfig.s3_images_bucket, image.filename, 
              function(err, result){ if (err) { cb(err);} }
            ),
            articlePromise
          ])
        })
        .then(function(promises){
          var image = promises[0];
          var container = promises[1];
          var item = promises[2];

          if (!item || !image) throw errorHandling.InvalidModelId();
          return item.images.destroy(image.id);
        })
        .then(function(item) {
          console.log(item);
          cb(null, item);
        })
        .catch(function (err) {
          cb(err);
        });
    };

    this.activate = function (req, res, cb) {
      var superItem = this;
      var mandatoryNotFound = false;
      var zonesNotFound = false;
      var categoriesNotFound = false;
      
      this.findById(req.params.id, {include: "images"})
        .then(function (item) {

          var mandatoriesPromise = _(hedwigconfig.image_tags)
            .filter(tag => tag.mandatory)
            .each(function(mandatory_tag){
              var mandatoryFound = _.find(item.__data.images, function(image){
                return image.tag === mandatory_tag.tag;
              });

              if(!mandatoryFound || mandatoryFound.length < 1){
                mandatoryNotFound = mandatoryNotFound || true;
                return false;
              }
            })

          var categorizationsPromise = app.models.ItemCategorization
            .find({where: {itemId: item.id}, include: "categorization"})
            .then(function(items){
              return _(items)
                .map(function(item){ return {"type": item.__data.categorization.type}})
                .countBy('type')
                .value()
            })
            .then(function(response){
              if(_.isEmpty(response) || !response.zona || response.zona < 1){
                zonesNotFound = true;
              }
              if(_.isEmpty(response) || !response.categoria || response.categoria < 1){
                categoriesNotFound = true;
              }
            })

            return Q.all([mandatoriesPromise, categorizationsPromise]);

        })
        .then(function(response){
          if(mandatoryNotFound) cb("No se puede publicar: faltan imagenes obligatorias");
          if(zonesNotFound) cb("No se puede publicar: falta asignar al menos una zona");
          if(categoriesNotFound) cb("No se puede publicar: faltan asignar al menos una categoria");

          superItem.changeStatus(req.params.id, true)
            .then(function(response){
              cb(null, response);
            })
        })
    };

    this.deactivate = function (req, res, cb) {
      this.changeStatus(req.params.id, false)
        .then(function(response){
          cb(null, response);
        })
    };

    this.locked = function (req, res, cb) {
      return this.findById(req.params.id)
        .then(function (item) {
          cb(null, item.locked);
        })
        .catch(function(err){
          return cb(err);
        });
    };

    this.lock = function (req, res, cb) {
      return this.changeLock(req.params.id, req.body.userId);
    };

    this.unlock = function (req, res, cb) {
      return this.changeLock(req.params.id, 0);
    };

    this.changeLock = function(itemId, lock){
      return this.findById(itemId)
        .then(function (item) {
          if(item.locked === lock){
            return {'status': 'ok'};
          } else {
            return item.updateAttributes({ 'locked': lock })
              .then(function(item){
                return {'status': 'ok'};
              });
          }
        })
        .catch(function(err){
          return {'status': 'not ok', 'error': err};
        });
    };

    this.changeStatus = function(itemId, status){
      console.log(itemId);

      return this.findById(itemId)
        .then(function (item) {
          console.log(item);
          if(item.active === status){
            return item
          } else {

            var data = {
              'active': status,
              'publication_change_date': new Date()
            };

            return item.updateAttributes(data)
              .then(function(item){
                return item;
              })
          }
        })
        .catch(function(err){
          console.log(err);
          return err;
        })
    };

    function getByStatus(status, This){
      return This.find({where: {active: status}})
        .then(function(items){
          return items;
        })
        .catch(function(err){
          return err;
        })
    }

    this.inactive = function(cb){
      getByStatus(false, this)
        .then(function(items){
          cb(null, items);
        })
        .catch(function(err){
          console.log(err);
          return err;
        });
    };

    this.active = function(cb){
      getByStatus(true, this)
        .then(function(items){
          cb(null, items);
        })
        .catch(function(err){
          console.log(err);
          return err;
        });
    };

    this.inPublishTime = function (req, res, cb) {
      Item.findById(req.params.id)
        .then(function(item){

          var publish_date_start = item['publish_date_start'];
          var publish_date_end = item['publish_date_end'];
          var currentDate = new Date();

          cb(null, ((publish_date_start <= currentDate) && (currentDate <= publish_date_end )) )
        })
    };

      // var mySQLDataSource = app.dataSources.mysql;
      // var mySQLPool = mySQLDataSource.connector.client;
      // console.log('SELECT * FROM \''+ hedwigconfig.item_type +'\' WHERE MATCH(title, body) AGAINST (\'' + req.params.searchQuery + '\')');
      // mySQLPool.query('SELECT * FROM \''+ hedwigconfig.item_type +'\' WHERE MATCH(\'title\', \'body\') AGAINST (\'' + req.params.searchQuery + '\')', function(err, rows, fields) {
      //   if (err) throw err;
	  //
      //   console.log("row", row);
      //   console.log("fields", fields);
      // });

    function executeSearch(model, count, req, res, cb){
      var filterObject = req.query.filter ? req.query.filter : {};
      var searchQuery = req.params.searchQuery;

      _.extend(filterObject, {where: {or: [{title: {like: '%' + searchQuery + '%'}},{body: {like: '%' + searchQuery + '%'}}]}});

      model.find( filterObject )
        .then(function(items){
          var result = count ? {count: items.length} : items;
          cb(null, result);
        })
        .catch(function(err){
          console.log(err);
          cb(err);
        })
    }

    this.search = function (req, res, cb) {
      executeSearch(this, false, req, res, cb)
    };

    this.remoteMethod(
      'search',
      {
        http: {path: '/search/:searchQuery', verb: 'get'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: '[this]', root: true}
      }
    );

    this.searchCount = function (req, res, cb) {
      executeSearch(this, true, req, res, cb)
    };

    this.remoteMethod(
      'searchCount',
      {
        http: {path: '/searchCount/:searchQuery', verb: 'get'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: '[this]', root: true}
      }
    );


    this.remoteMethod(
      'deleteImage',
      {
        http: {path: '/:id/deleteImage/:imageID', verb: 'delete'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'object'}
      }
    );

    this.remoteMethod(
      'uploadImage',
      {
        http: {path: '/:id/uploadImage/:tag', verb: 'post'},
        accepts: [
          {arg: 'id', type: 'number', required: true},
          {arg: 'tag', type: 'string'},
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'image', root: true}
      }
    );
    this.remoteMethod(
      'massiveLoad',
      {
        http: {path: '/massiveLoad/:id', verb: 'post'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'image', root: true}
      }
    );

    this.remoteMethod(
      'activate',
      {
        http: {path: '/:id/activate/', verb: 'post'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'this', root: true}
      }
    );

    this.remoteMethod(
      'deactivate',
      {
        http: {path: '/:id/deactivate/', verb: 'post'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'this', root: true}
      }
    );

    this.remoteMethod(
      'locked',
      {
        http: {path: '/:id/locked/', verb: 'get'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'Object', root: true}
      }
    );

    this.remoteMethod(
      'lock',
      {
        http: {path: '/:id/lock/', verb: 'post'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'Object', root: true}
      }
    );

    this.remoteMethod(
      'unlock',
      {
        http: {path: '/:id/unlock/', verb: 'post'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'this', root: true}
      }
    );

    this.remoteMethod(
      'active',
      {
        http: {path: '/active/', verb: 'get'},
        returns: {type: '[this]', root: true}
      }
    );

    this.remoteMethod(
      'inactive',
      {
        http: {path: '/inactive/', verb: 'get'},
        returns: {type: '[this]', root: true}
      }
    );

    this.remoteMethod(
      'inPublishTime',
      {
        http: {path: '/:id/inpublishtime/', verb: 'get'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}}
        ],
        returns: {type: 'boolean', root: true}
      }
    );


  };

  Item.setup();
};

