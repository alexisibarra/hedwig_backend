'use strict';

var _ = require('lodash');
var app = require('../../server/server');

module.exports = function(Linews) {
    Linews.inZones = function(req, res, count, zones, cb){
      var ItemCategorization = app.models.ItemCategorization;

      var filter = {
        include: "li-news"
      };
      if (zones){
        filter.where = {
          categorizationId: {
            inq: zones.split(',')
          }
        }
      }

      ItemCategorization
        .find(filter)
        .then(function(categories){
          var articlesSet = new Set();
          _.each(categories, function(category){
            var article = category['__data']['li-news'];

            if (!_.isUndefined(article) && article.active === true) {
              articlesSet.add(article);
            }
          });

          let articles = [...articlesSet];

          if(count){
            cb(null, _.take(articles,count))
          }else{
            cb(null, articles);
          }
        })
        .catch(function(err){
          console.log(err);
          return err;
        });
    };

    Linews.remoteMethod(
      'inZones',
      {
        http: {path: '/inzones/', verb: 'get'},
        accepts: [
          {arg: 'req', type: 'object', 'http': {source: 'req'}},
          {arg: 'res', type: 'object', 'http': {source: 'res'}},
          {arg: 'count', type: 'number'},
          {arg: 'zones', type: 'String'}
        ],
        returns: {type: '[this]', root: true}
      }
    );

    Linews.remoteMethod(
        'checkdestacado',
        {
            accepts: [
                {arg: 'req', type: 'object', 'http': {source: 'req'}},
                {arg: 'res', type: 'object', 'http': {source: 'res'}}
            ],
            http: {path: '/:id/checkdestacado/', verb: 'post'},
            returns: {type: 'Object', root: true}
        }
    );
    Linews.remoteMethod(
        'checknodestacado',
        {
            accepts: [
                {arg: 'req', type: 'object', 'http': {source: 'req'}},
                {arg: 'res', type: 'object', 'http': {source: 'res'}}
            ],
            http: {path: '/:id/checknodestacado/', verb: 'post'},
            returns: {type: 'Object', root: true}
        }
    );
};
