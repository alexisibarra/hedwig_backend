'use strict';

module.exports = function(Tweet) {
  Tweet.beforeRemote('create', function (context, alert, next) {
    context.req.body.type = "tweet";
    next();
  });

};
