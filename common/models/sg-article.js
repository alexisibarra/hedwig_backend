'use strict';

module.exports = function(SG_Article) {
  SG_Article.validatesPresenceOf('publish_date_start');
  SG_Article.validatesPresenceOf('publish_date_end');

  this.beforeRemote('*.__create__tweet', function (context, alert, next) {
    context.req.body.type = "tweet";
    next();
  });

  this.beforeRemote('*.__create__youtube', function (context, alert, next) {
    context.req.body.type = "youtube";
    next();
  });
};
