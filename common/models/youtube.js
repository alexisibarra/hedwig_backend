'use strict';

module.exports = function(Youtube) {
  Youtube.beforeRemote('create', function (context, alert, next) {
    context.req.body.type = "youtube";
    next();
  });
};
