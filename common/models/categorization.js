'use strict';
var _ = require('lodash');

module.exports = function(Categorization) {
  Categorization.validatesUniquenessOf('body', {message: 'El nombre elegido ya está en uso'});

  var getCategorizationType = function (type, req, res, cb) {
    var limit;
    var skip; 
    if(req.query.filter){
      limit = req.query.filter.limit;
      skip = req.query.filter.skip;
    }
    return Categorization.find({where: {type: type}, order: 'position ASC', limit: limit, skip: skip})
      .then(function(Categorizations){
        if (!Categorizations){
          throw errorHandling.InvalidModelId();
        } else{
          cb(null, Categorizations);
        }
      })
      .catch(function (err) {
        cb(err);
      });
  };

  Categorization.getZones = function (req, res, cb) {
    getCategorizationType("zona", req, res, cb);
  }

  Categorization.getCategories = function (req, res, cb) {
    getCategorizationType("categoria", req, res, cb);
  }

  Categorization.getCensuras = function (req, res, cb) {
    getCategorizationType("censura", req, res, cb);
  }

  Categorization.types = function (req, res, cb) { 
    Categorization.find() 
      .then(function(Categorizations){ 
          let result = _(Categorizations) 
            .groupBy("type") 
            .keys() 
            .value(); 
 
          cb(null, result); 
      }) 
      .catch(function (err) { 
        cb(err); 
      }); 
  }; 

  Categorization.remoteMethod(
    'getZones',
    {
      http: {path: '/zones/', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: '[Categorization]', root: true}
    }
  );

  Categorization.remoteMethod(
    'getCategories',
    {
      http: {path: '/categories/', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: '[Categorization]', root: true}
    }
  );
  Categorization.getZones = function (req, res, cb) {
    Categorization.find({where: {type: 'zona'}, order: 'position ASC'})
      .then(function(Categorizations){
        if (!Categorizations){
          throw errorHandling.InvalidModelId();
        } else{
          cb(null, Categorizations);
        }
      })
      .catch(function (err) {
        cb(err);
      });
  };
  Categorization.types = function (req, res, cb) {
    Categorization.find()
      .then(function(Categorizations){
          let result = _(Categorizations)
            .groupBy("type")
            .keys()
            .value();

          cb(null, result);
      })
      .catch(function (err) {
        cb(err);
      });
  };

  Categorization.remoteMethod(
    'getCensuras',
    {
      http: {path: '/censuras/', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: '[Categorization]', root: true}
    }
  );

  Categorization.remoteMethod(
    'types',
    {
      http: {path: '/types', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: 'Object', root: true}
    }
  );
};
