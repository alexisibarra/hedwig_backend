'use strict';

var app = require('../../server/server');
var _ = require('lodash');
var hedwigconfig = app.settings.backend;

module.exports = function(Editoruser) {
  Editoruser.validatesUniquenessOf('email', {message: 'El email indicado ya está en uso'});
  Editoruser.validatesUniquenessOf('username', {message: 'El nombre de usuario indicado ya está en uso'});

  Editoruser.beforeRemote('create', function (context, alert, next) {
    context.req.body['password_change'] = false;
    context.req.body.active = false;

    next();
  });

  var plainPwd
  Editoruser.beforeRemote( 'create', function (ctx, inst, next) {
      plainPwd = ctx.req.body.password
      next()
  })

  Editoruser.validate( 'password', function (err, res) {
    if (plainPwd && plainPwd.length < 8 ) err()
  }, { message: 'Invalid format' }) 

  Editoruser.afterRemote('login', function (context, alert, next) {

    Editoruser.findById(context.result.userId, {"include": "roles"})
      .then(function(user){

        Editoruser.fetchUserRole(alert.userId)
          .then(function (role) {
            context.result.role = role.role;
            context.result.fullname = user.fullname;
            return next(null);
          })
          .catch(function(err){
            console.log(err);
          });

      });

  });

  Editoruser.fetchUserRole = function(uid) {
    // User roles are based on the native Loopback RoleMapping relations,
    // or on the MarinaAdmin mapping (this last one is specific to SmartSea).
    return new Promise(function(resolve, reject) {
      // First, try to find if a generic RoleMapping exists for this user.
      app.models.RoleMapping.findOne({'where': { 'principalId': uid}, include: {relation: 'role'} })
        .then(function(roleMapping) {
          if (roleMapping != null) {
            // We found a Loopback role mapping, so return role based on that.
            var roleName = roleMapping.toObject().role.name;
            resolve( {role: _.capitalize(roleName)} );
          } else {
            resolve({role: 'Editor'});
          }
        })
        .catch(function(err){
          console.log(err);
        });
    });
  };

  Editoruser.on('resetPasswordRequest', function(info) {
    var url = app.settings.backend.frontend_URL + '/#!/resetpassword';
    var html = 'Haga click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">acá</a> para reestablecer su contraseña';

    app.models.Mailer.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('> error sending password reset email: ', err);
      console.log('> sending password reset email to:', info.email);
    });
  });

  function executeSearch(count, req, res, cb){
    var filterObject = req.query.filter ? req.query.filter : {};
    var searchQuery = req.params.searchQuery;

    _.extend(filterObject, {where: {or: [{fullname: {like: '%' + searchQuery + '%'}},{username: {like: '%' + searchQuery + '%'}},{email: {like: '%' + searchQuery + '%'}}]}});

    Editoruser.find( filterObject )
      .then(function(items){
        var result = count ? {count: items.length} : items;
        cb(null, result);
      })
      .catch(function(err){
        console.log(err);
        cb(err);
      })
  }

  Editoruser.search = function (req, res, cb) {
    executeSearch(false, req, res, cb)
  };

  Editoruser.remoteMethod(
    'search',
    {
      http: {path: '/search/:searchQuery', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: '[this]', root: true}
    }
  );

  Editoruser.searchCount = function (req, res, cb) {
    executeSearch(true, req, res, cb)
  };

  Editoruser.remoteMethod(
    'searchCount',
    {
      http: {path: '/searchCount/:searchQuery', verb: 'get'},
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {type: '[this]', root: true}
    }
  );

};
