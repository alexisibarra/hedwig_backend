# 1.1.5
- [feature] Mensajes de error en espanol para categorizacion y usuarios
- [feature] Marcar articulo como destacado
# 1.2.0
- [feature] Manejo de restauración de contraseñas
- [feature] expone en (GET)/api/ información de configuración para el frontend
- [feature] Hace uso de archivos de configuración en vez del .env

# 1.2.1
- [bugfix] Mensaje de recuperación de contraseña en español

# 1.3.1
- [feature] Añade fixtures para categorizations
- [feature] Se modifica el endpoint massiveLoad para incluir el id del usuario al cual se le adjudicara la autoria de los articulos
- [feature] Diferencia los fixtures de categorizaciones según el cliente

# 1.4.0
- [feature] Busqueda de usuarios
- [feature] Contabilizacion busqueda de usuarios
- [feature] Busqueda de items
- [feature] Contabilizacion busqueda de items

# 1.4.1
- [feature] Añade campo de configuración para propiedad colectiva
- [feature] Añade campo de creditos para li-news

#1.5.0
- [feature] Usa MongoDB como motor de base de datos
- [feature] Implementa el endpoint 'li-news/inZones' para la integración con fenix

#1.6.0
- [feature] cambia la propiedad destacado de las li-news a una zona y elimina los metodos relacionados a este

#1.6.1
- [feature] Anade manejo de froala e incorpora un metodo en el modelo image para obtener el hash de s3 necesario para subir imagenes con froala"

#1.6.2
- [feature] cambia la etiqueta de configuración del bucket de imagenes s3 a 's3_images_bucket'
