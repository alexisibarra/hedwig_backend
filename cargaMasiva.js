var XLSX = require('xlsx');
var _ = require('lodash');


function parseFile(fileDir) {
  var workbook = XLSX.readFile(fileDir);

  var sheet_name_list = workbook.SheetNames;
  var items = [];

  _.each(sheet_name_list, function(sheetName) {
    var worksheet = workbook.Sheets[sheetName];

    // Se ubica el rango de celdas con información útil del archivo
    var worksheetRange = _.find(worksheet, function (item, key) {
      return key[0] === '!';
    }).split(':');

    var columnInit = worksheetRange[0][0].charCodeAt(0);
    var columnEnd = worksheetRange[1][0].charCodeAt(0);
    var rowInit = _.toInteger(worksheetRange[0][1]);
    var rowEnd = _.toInteger(worksheetRange[1][1]);

    _.each(
      _.range(columnInit, columnEnd + 1),
      columnNumber => {
        var fieldnameCoordinate = String.fromCharCode(columnNumber) + 1;
        _.each(
          _.range(rowInit + 1, rowEnd + 1),
          rowNumber => {
            var valueCoordinate = String.fromCharCode(columnNumber) + rowNumber;
            // Se eliminó una fila y xlsx comienza a contar en 1, asi que...
            rowNumber -= 2;

            if (!items[rowNumber]) {
              items[rowNumber] = {};
            }

            var fieldname = worksheet[fieldnameCoordinate].w;

            items[rowNumber][fieldname] = worksheet[valueCoordinate].w;
          }
        );
      }
    );
  });

  return items;
}

console.log(parseFile("./uploads/cargaMasiva.ods"));

